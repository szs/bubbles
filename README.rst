Bubbles
=======

A simple yet fun pastime for small children.

Install and run
---------------

Install poetry following `these instructions <https://python-poetry.org/docs/#installation>`_.

Download the tar ball and unpack or clone the git repository. Change into the directory ``bubbles``.

Run

::

 poetry install

to install the dependencies.

Run the program with

::

 poetry run bubbles/bubbles.py

How to play
-----------

The game starts in mode "create". In this mode mouse clicks in the window will create new bubbles.
Change the mode to "burst" using the ``SPACE`` key to burst the bubbles.

To quit the game, press the ``q`` key.
