#!/usr/bin/env python3
"""bubbles
USAGE
   $ bubbles
COPYRIGHT
   (C) 2020, Steffen Brinkmann <s-b@mailbox.org>
LICENCE
   Published under the terms of the MIT License, see https://mit-license.org/
"""

import math
import logging
from random import randint
import sys
from math import sqrt
from typing import Set, Tuple, Optional

import pygame
from pygame import Color
from pygame.locals import K_q, K_SPACE

debug = False


class Bubble:
    dt = 0.01
    n_id = 0

    def __init__(self, position, velocity, size=None, width=None, colour=None):
        self.id = self.n_id
        Bubble.n_id += 1
        self.colour = colour if colour else self.get_random_colour()
        self.position = position
        self.velocity = velocity
        self.wiggle_count = 0
        self.size = size if size else self.get_random_size()
        self.width = width if width else self.get_random_width()
        self.wiggle_time = 50 + self.size**1.2 * 2
        self.wiggle_width = 10 + 3 * sqrt(self.size) / 3

    def update(self, screen, fill=False):
        self.wiggle_count += 1
        wiggle = (math.sin(2 * math.pi / self.wiggle_time * self.wiggle_count)
                  * self.wiggle_width)
        self.position = (self.position[0] + self.velocity[0] * self.dt,
                         self.position[1] + self.velocity[1] * self.dt)
        int_pos = int(self.position[0] + wiggle), int(self.position[1])
        width = self.width if fill is False else 0
        pygame.draw.circle(screen, self.colour, int_pos, self.size, width)
        screen.blit(font_24.render(str(self.id), True, (180, 180, 0)),
                    dest=(int_pos[0] - 5, int_pos[1] - 10))

    def get_distance(self, pos):
        return sqrt((pos[0] - self.position[0])**2
                    + (pos[1] - self.position[1])**2)

    @classmethod
    def get_random_colour(self):
        return Color(randint(0, 255), randint(0, 255), randint(0, 255), randint(0, 255))

    @classmethod
    def get_random_size(self):
        return randint(10, 200)

    @classmethod
    def get_random_width(self):
        return randint(1, 5)


def get_closest_bubble(bubbles: Set[Bubble], pos: Tuple[int, int]) -> Optional[Bubble]:
    try:
        closest = next(iter(bubbles))
    except StopIteration:
        return

    min_distance = closest.get_distance(pos)

    for b in bubbles:
        b_distance = b.get_distance(pos)
        if b_distance < min_distance:
            min_distance = b_distance
            closest = b
    return closest


def burst(bubbles: Set[Bubble], pos: Tuple[int, int], r_factor: float = 1) -> None:
    b = get_closest_bubble(bubbles, pos)
    if b is not None:
        distance = b.get_distance(pos)
        if distance * r_factor <= b.size:
            bubbles.remove(b)


if __name__ == '__main__':
    # configuration

    logging.basicConfig(level=logging.DEBUG,
                        format="%(asctime)s %(levelname)10s %(threadName)s | %(message)s")
    # datefmt='%m-%d %H:%M',
    # filename=log_file,
    # filemode=filemode_val

    if not pygame.font:
        logging.warning('Warning, fonts disabled')
    if not pygame.mixer:
        logging.warning('Warning, sound disabled')

    logging.info(f'Arguments: {sys.argv}')

    pygame.init()

    size = width, height = 1000, 800
    speed = [2, 2]
    black = 0, 0, 0
    mode = 'create'  # can be 'create' or 'burst'

    screen = pygame.display.set_mode(size)
    font_36 = pygame.font.Font(pygame.font.get_default_font(), 36)
    font_24 = pygame.font.Font(pygame.font.get_default_font(), 24)

    n_bubbles = 1
    bubbles = set()
    bubbles.add(Bubble((500, 700), (10, -10), 30, 1, Color(145, 155, 255, 127)))
    closest = None

    while 1:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                sys.exit()
            if event.type == pygame.KEYDOWN:
                if event.key == K_q:
                    sys.exit()
            if event.type == pygame.MOUSEMOTION:
                if mode == 'burst':
                    closest = get_closest_bubble(bubbles, pygame.mouse.get_pos())
                    # logging.debug(f'MOUSEMOTION: {pygame.mouse.get_pos()} {closest.id}')
            if event.type == pygame.MOUSEBUTTONDOWN:
                if mode == 'create':
                    size = Bubble.get_random_size()
                    bubbles.add(Bubble(pygame.mouse.get_pos(), (0, -1000 / size), size))
                if mode == 'burst':
                    burst(bubbles, pygame.mouse.get_pos(), r_factor=1)
            if event.type == pygame.KEYDOWN:
                if event.key == K_SPACE:
                    mode = 'burst' if mode == 'create' else 'create'

        screen.fill(black)

        to_be_deleted = set()
        for b in bubbles:
            if (b.position[1] + b.size < 0
                or b.position[1] - b.size > height
                or b.position[0] + b.size < 0
                or b.position[0] - b.size > width):
                to_be_deleted.add(b)
                continue
            b.update(screen)

        if debug is True:
            if closest is not None:
                for b in bubbles:
                    if b.id == closest.id:
                        b.update(screen, fill=True)
                        break

        bubbles -= to_be_deleted

        # text display
        screen.blit(font_36.render(mode, True, (180, 0, 0)), dest=(20, 10))
        screen.blit(font_36.render(str(len(bubbles)), True, (180, 0, 0)), dest=(width - 50, 10))

        pygame.display.flip()
